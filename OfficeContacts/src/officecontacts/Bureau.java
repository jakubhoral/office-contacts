package officecontacts;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

public class Bureau implements Serializable {

    private DefaultListModel<String> model = new DefaultListModel<>();

    protected ArrayList<Osoba> workers = new ArrayList<>();

    public ArrayList<Osoba> getWorkers() {
        return workers;
    }

    public void initializeListModel() {
        for (Osoba worker : workers) {
            model.addElement(worker.toString());
        }
    }

    public DefaultListModel<String> getModel() {
        return model;
    }

    public void deleteWorkers() {
        this.workers.clear();
    }

    public void loadFromCSV() {
        File f = new File("office.csv");

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "windows-1250"))) {

            String line = "";

            while ((line = br.readLine()) != null) {
                ArrayList<String> row = new ArrayList<String>();
                for (String str : line.split(";")) {
                    row.add(str.trim());
                }
                workers.add(new Osoba(row.get(0), row.get(1),
                        row.get(2), row.get(3), row.get(4), row.get(5),
                        row.get(6), row.get(7), row.get(8), row.get(9)));
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Bureau.class.getName()).log(Level.SEVERE, null, ex);

        } catch (IOException ex) {
            Logger.getLogger(Bureau.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void printdataToConsole() {
        for (Osoba worker : workers) {
            System.out.println(worker);
        }
    }

    public void exportToCSV() {
        try (BufferedWriter bw = Files.newBufferedWriter(Paths.get("test.csv"),
                Charset.forName("windows-1250"))) {
            for (Osoba worker : workers) {
                bw.write(String.join(";", worker.getDataArray()));
                bw.write(";\r\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(Bureau.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public ArrayList<Osoba> search(String type, String inp) throws PersonNotFoundException {
        ArrayList<Osoba> match = new ArrayList<Osoba>();

        for (Osoba worker : workers) {
            if (worker.getName().contains(inp) && type.equals("name")) {
                match.add(worker);
            } else if (worker.getMobil().contains(inp) && type.equals("mobile")) {
                match.add(worker);
            } else if (worker.getProfession().contains(inp) && type.equals("profession")) {
                match.add(worker);
            } else if (worker.getDescription().contains(inp) && type.equals("description")) {
                match.add(worker);
            } else if (worker.getNumDoor().contains(inp) && type.equals("numDoor")) {
                match.add(worker);
            } else if (worker.getPhone().contains(inp) && type.equals("phone")) {
                match.add(worker);
            } else if (worker.getEmail().contains(inp) && type.equals("email")) {
                match.add(worker);
            } else if (worker.getDepartment().contains(inp) && type.equals("department")) {
                match.add(worker);
            } else if (worker.getUnit().contains(inp) && type.equals("unit")) {
                match.add(worker);
            } else if (worker.getNameUnit().contains(inp) && type.equals("nameUnit")) {
                match.add(worker);
            }
        }
        if (match.isEmpty()) {
            throw new PersonNotFoundException(type + " not found");
        }
        return match;
    }

    public void edit(String dataToChange, String type, String newData) throws PersonNotFoundException, ToManyPersons {
        if (search(type, dataToChange).size() != 1) {
            throw new ToManyPersons("To many people find, specify criteria");
        }
        edit(search(type, dataToChange).get(0), type, newData);
    }

    public void edit(Osoba worker, String type, String newData) {
        switch (type) {
            case "name":
                worker.setName(newData);
                break;
            case "mobile":
                worker.setMobil(newData);
                break;
            case "profession":
                worker.setProfession(newData);
                break;
            case "description":
                worker.setDescription(newData);
                break;
            case "numDoor":
                worker.setNumDoor(newData);
                break;
            case "phone":
                worker.setPhone(newData);
                break;
            case "email":
                worker.setEmail(newData);
                break;
            case "department":
                worker.setDepartment(newData);
                break;
            case "unit":
                worker.setUnit(newData);
                break;
            case "nameUnit":
                worker.setNameUnit(newData);
                break;
        }        
    }

    public void saveTOFile() {

        try {
            FileOutputStream fileOut = new FileOutputStream("Bareau.bit");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved in Bareau.bit\n");
        } catch (IOException i) {
            i.printStackTrace();
        }
    }

    public void loadFromFile() {

        try {
            FileInputStream fileIn = new FileInputStream("Bareau.bit");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            Bureau bur = (Bureau) in.readObject();
            this.workers = bur.getWorkers();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Employee class not found");
            c.printStackTrace();
            return;
        }

    }

}
