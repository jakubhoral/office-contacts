package officecontacts;

import java.io.Serializable;
import java.util.ArrayList;

public class Osoba implements Serializable{

private String name;
private String profession;
private String description;
private String numDoor;
private String phone;
private String mobil;
private String email;
private String department;
private String unit;
private String nameUnit;
private ArrayList<String> dataArray = new ArrayList<String>();






    public Osoba(String name, String profession, String description, String numDoor, String phone, String mobil, String email, String department, String unit, String nameUnit) {
        this.name = name;
        dataArray.add(name);
        this.profession = profession;
        dataArray.add(profession);
        this.description = description;
        dataArray.add(description);
        this.numDoor = numDoor;
        dataArray.add(numDoor);
        this.phone = phone;
        dataArray.add(phone);
        this.mobil = mobil;
        dataArray.add(mobil);
        this.email = email;
        dataArray.add(email);
        this.department = department;
        dataArray.add(department);
        this.unit = unit;
        dataArray.add(unit);
        this.nameUnit = nameUnit;
        dataArray.add(nameUnit);
    }

    public Osoba() {
    }
    
  


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumDoor() {
        return numDoor;
    }

    public void setNumDoor(String numDoor) {
        this.numDoor = numDoor;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobil() {
        return mobil;
    }

    public void setMobil(String mobil) {
        this.mobil = mobil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getNameUnit() {
        return nameUnit;
    }

    public void setNameUnit(String nameUnit) {
        this.nameUnit = nameUnit;
    }

    public ArrayList<String> getDataArray() {
        return dataArray;
    }
    
    

    @Override
    public String toString() {
        return  name  +
                ", Profession: " + profession +
                ", Description: " + description +
                ", numDoor: " + numDoor  +
                ", phone: " + phone  +
                ", mobil: " + mobil  +
                ", email: " + email  +
                ", department: " + department  +
                ", unit: " + unit  +
                ", nameUnit: " + nameUnit ;
    }
}
